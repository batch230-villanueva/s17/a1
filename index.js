console.log("Hello, World")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    const displayUserDetails = () => {
        let fullName = prompt("Enter your full name: ");
        let age = prompt("Enter your age: ");
        let location = prompt("Enter your location: ");

        alert("Thanks for providing your info");

        console.log("=========================");
        console.log("USER INFO");
        console.log("=========================");
        console.log(`Full Name: ${fullName}`);
        console.log(`Age: ${age}`);
        console.log(`Location: ${location}`);
        console.log("=========================");
    }

    displayUserDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

    const displayFaveBands = () => {
        console.log("=========================");
        console.log("MY FAVOURITE BANDS:");
        console.log("=========================");
        console.log("1.) Cocteau Twins");
        console.log("2.) Death Grips");
        console.log("3.) Medyo Maybe");
        console.log("4.) Istochnik");
        console.log("5.) Sonic Death");
        console.log("=========================");
    }

    displayFaveBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	

*/
	
	//third function here:

const displayFaveMovies = () => {
    console.log("=========================");
    console.log("MY FAVOURITE MOVIES:")
    console.log("=========================");
    console.log("1.) Ivan's Childhood/ My Name is Ivan (1963)");
    console.log("Rotten Tomatoes Rating: 100%");
    console.log("2.) Nightcrawler (2014)");
    console.log("Rotten Tomatoes Rating: 95%");
    console.log("3.) Taxi Driver (1976)");
    console.log("Rotten Tomatoes Rating: 96%");
    console.log("4.) Drive (2011)");
    console.log("Rotten Tomatoes Rating: 93%");
    console.log("5.) Ratatouille (2007)");
    console.log("Rotten Tomatoes Rating: 96%");
    console.log("=========================");
}

displayFaveMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);